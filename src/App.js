import './less/app.less';
import '../src/less/app.less';
import '../src/views/landing'
import LandingView from '../src/views/landing';

function App() {
  return (
    <div className="App">
      <LandingView/>
    </div>
  );
}

export default App;
