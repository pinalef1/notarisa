import React from 'react';
import { Row, Col, Typography } from 'antd';
import armasLogo from '../img/logo/armas-logo.svg';
import galileaLogo from '../img/logo/galilea-logo.svg';
import icuadraLogo from '../img/logo/icuadra-logo.svg';
import salmonesLogo from '../img/logo/salmones-logo.svg';
import totalAbLogo from '../img/logo/totalAbogados-logo.svg';
import zonaProLogo from '../img/logo/zonaPropia-logo.svg';

const { Text } = Typography;

const BrandsSection = () => {

  return (
    <>
      <div className="bg-gray-2">
        <br/>
        <Row justify="center">
          <Col>
            <Text type="secondary" className="text-center small">YA TRABAJAN CON NOSOTROS</Text>
          </Col>
        </Row>
        <br/>
        <Row justify="center" gutter={{ lg: 72 }}>
          <Col>
            <img src={galileaLogo} alt="logo" />
          </Col>
          <Col>
            <img src={icuadraLogo} alt="logo" />
          </Col>
          <Col>
            <img src={salmonesLogo} alt="logo" />
          </Col>
          <Col>
            <img src={totalAbLogo} alt="logo" />
          </Col>
          <Col>
            <img src={zonaProLogo} alt="logo" />
          </Col>
          <Col>
            <img src={armasLogo} alt="logo" />
          </Col>
        </Row>
        <br/>
        <br/>
      </div>
    </>
  );
};

export default BrandsSection;
