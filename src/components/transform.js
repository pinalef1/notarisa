import React from 'react';
import { Row, Col, Typography, Card, Button } from 'antd';

import backgroundImg from '../img/bg/orange-bg.svg';

const { Title, Paragraph } = Typography;

const TransformSection = () => {

  return (
    <>
      <Row justify="center">
        <Col md={22}>
          <Card className="bg-gray-2" style={{ height: '340px' }}>
            <br />
            <br />
            <Row justify="center">
              <Col lg={16}>
                <Paragraph className="text-center fw-medium ff-poppins text-volcano-10">NOSOTROS</Paragraph>
                <Title className="text-center" style={{ marginTop: 12 }}>La transformación digital está aquí</Title>
              </Col>
            </Row>
            <Row justify="center">
              <Col lg={12}>
                <Title level={4} className="fw-regular text-center" style={{ fontSize: '20px' }}>En cualquier lugar y en cualquier momento, simplifica tus negocios y potencia la relación con tus clientes.</Title>
              </Col>
              </Row>
            <br />
            <Row justify="center">
              <Col>
                <Button className="btn-volcano-7" size="large" style={{ padding: '8px 16px' }}>La transformación digital está aquí</Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <br />
      <br />

    </>
  );
};

export default TransformSection;
