import React from 'react';
import { Row, Col, Typography, Card, Button } from 'antd';

import backgroundBanner from '../img/bg/bg.svg';
import grafImg from '../img/graficas.svg';

const { Title } = Typography;

const HeaderSection = () => {

  return (
    <>
      <Row justify="center">
        <Col md={22}>
          <Card className="bg-gray-2" style={{ height: '340px' }}>
            <br />
            <br />
            <Row justify="center">
              <Col md={18}>
                <Title className="text-center fw-medium">Firma aquí tus documentos digitales: rápido, seguro y autorizado por un notario</Title>
              </Col>
            </Row>
            <br />
            <Row justify="center">
              <Col>
                <Button className="btn-gradient" size="large" style={{ padding: '8px 16px' }}>Agendar una demo</Button>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <br />
      <br />
      <Row justify="center">
        <Col md={22}>
          <Card style={{
            backgroundImage: `url(${backgroundBanner})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            marginBottom: '1rem',
            backgroundColor: '#fff1f0',
            height: '318px',
            position: 'relative',
          }}>
            <img src={grafImg} alt="graficas" style={{
              position: 'absolute', top: '48%',
              left: '50%',
              marginTop: '-230px',
              marginLeft: '-440px'
            }} />
          </Card>
        </Col>
      </Row>
      <br />
    </>
  );
};

export default HeaderSection;
